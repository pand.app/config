{ config, pkgs, ... }:
{
  imports = [
    ./neovim.nix
  ];

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "25.05"; # Please read the comment before changing.

  home.packages = with pkgs; [
    # General Utilities
    sq # like jd but for SQL queries
    sd # Simplified 'sed' for easier find and replace
    ox # Minimalist text editor written in Rust, alternative to nano
    dust # More intuitive version of 'du', for disk usage analysis
    procs # Enhanced 'ps' alternative, provides detailed process information
    tokei # Tool to count code lines, etc., in multiple programming languages
    dasel # Select, put and delete data from JSON, YAML, TOML, XML and CSV files
    grex # Tool to generate regular expressions from user-provided test cases
    hyperfine # Command-line benchmarking tool
    graphviz # Utility for generating visual representations of graphs in .dot format
    translate-shell # Command-line translator using Google Translate
    ffmpeg # Comprehensive multimedia framework to handle audio and video files
    gnupg # Encryption and signing tool to secure communications
    gnused # GNU version of the stream editor 'sed'
    translate-shell
    yazi # File manager written in Rust
    macchina # Information fetcher in rust
    spacer # insert spacers when command output stops

    # Networking Tools
    bandwhich # Network bandwidth analysis tool
    dogdns # Command-line DNS client
    httpie # User-friendly cURL replacement to make CLI HTTP requests
    curl # Command line tool and library for transferring data with URLs
    wget # Free utility for non-interactive download of files from the web
    nmap # Network discovery and security auditing tool
    websocat # Like socat, but for WebSockets

    # Development Tools
    glab # GitLab command-line interface tool to interact with GitLab API
    gh # GitHub CLI to manage GitHub repositories
    k6 # Modern load testing tool, primarily for web applications
    shellcheck # Check bash syntax

    # Security tools
    trivy # Scanner
    _1password-cli # 1Password CLI

    # DevOps Tools
    terraform # Infrastructure as code software tool
    scaleway-cli # CLI to manage Scaleway cloud services
    kubectl # CLI to manage Kubernetes clusters
    kubernetes-helm # Package manager for Kubernetes
    kubectx # Tool to switch between Kubernetes contexts
  ];

  home.file = { };

  # General Utilities
  programs.pandoc = {
    enable = true;
  };
  # Alternative to ls for listing directory contents
  programs.eza = {
    enable = true;
    enableFishIntegration = true;
  };
  # Extremely fast text search, similar to 'grep'
  programs.ripgrep = {
    enable = true;
  };
  # User-friendly alternative to 'find' for file system navigation
  programs.fd = {
    enable = true;
  };
  # Terminal workspace and multiplexer, alternative to tmux
  programs.zellij = {
    enable = true;
    enableFishIntegration = false;
    settings = {
      copy_on_select = false;
    };
  };
  # Simplified, faster 'tldr' client written in Rust
  programs.tealdeer = {
    enable = true;
    settings = {
      display = {
        compact = false;
      };
      updates = {
        auto_update = false;
      };
    };
  };

  # Alternative to cat https://github.com/sharkdp/bat
  programs.bat = {
    enable = true;
  };
  programs.git = {
    enable = true;
    aliases = {
      up = "pull --rebase --autostash";
      clone-branches = ''! git branch -a | sed -n "/\/HEAD /d; /\/master$/d; /remotes/p;" | xargs -L1 git checkout -t'';
    };
    userName = "pandapp";
    lfs.enable = true;
    extraConfig = {
      pull.rebase = false;
      init.defaultBranch = "develop";
      core = {
        autocrlf = "input";
        fsmonitor = true;
        untrackedcache = true;
      };
      push.autoSetupRemote = true;
      status.showuntrackedfiles = "all";
      rebase.updateRefs = true;
    };
    ignores = [
      ".DS_STORE"
    ];
    signing = {
      format = "ssh";
      signByDefault = true;
    };
  };
  programs.fish = {
    enable = true;
    shellAbbrs = {
      cdgit = "cd (git rev-parse --show-toplevel)";
      gro = "git rebase origin/";
      gpf = "git push --force";
      ls = "eza";
      cat = "bat";
      k = "kubectl";
      ka = "kubectl apply --recursive -f";
      kex = "kubectl exec -i -t";
      klo = "kubectl logs -f";
      klop = "kubectl logs -f -p";
      kp = "kubectl proxy";
      kpf = "kubectl port-forward";
      kg = "kubectl get";
      kd = "kubectl describe";
      krm = "kubectl delete";
      krun = "kubectl run --rm --restart=Never --image-pull-policy=IfNotPresent -i -t";
      kgpo = "kubectl get pods";
      kdpo = "kubectl describe pods";
      krmpo = "kubectl delete pods";
      kgdep = "kubectl get deployment";
      kddep = "kubectl describe deployment";
      krmdep = "kubectl delete deployment";
      kgsvc = "kubectl get service";
      kdsvc = "kubectl describe service";
      krmsvc = "kubectl delete service";
      kging = "kubectl get ingress";
      kding = "kubectl describe ingress";
      krming = "kubectl delete ingress";
      kgcm = "kubectl get configmap";
      kdcm = "kubectl describe configmap";
      krmcm = "kubectl delete configmap";
      kgsec = "kubectl get secret";
      kdsec = "kubectl describe secret";
      krmsec = "kubectl delete secret";
      kgno = "kubectl get nodes";
      kdno = "kubectl describe nodes";
      kgns = "kubectl get namespaces";
      kdns = "kubectl describe namespaces";
      krmns = "kubectl delete namespaces";
      kgf = "kubectl get --recursive -f";
      kdf = "kubectl describe --recursive -f";
      krmf = "kubectl delete --recursive -f";
    };
    shellAliases = {
      ecoindex = "podman run -it --rm -v /tmp/ecoindex-cli:/tmp/ecoindex-cli vvatelot/ecoindex-cli:latest ecoindex-cli";
    };
    functions = {
      fish_greeting = "macchina";
      sleep_until = {
        description = "Wait until a specified hour";
        body = ''
          if test (count $argv) -ne 1
              echo "Usage: sleep_until HH:MM"
              return 1
          end

          set -l target_time $argv[1]
          set -l target_hour (string split ':' $target_time)[1]
          set -l target_minute (string split ':' $target_time)[2]

          if not string match --quiet --regex '^[0-9]{2}:[0-9]{2}$' -- $target_time
              echo "Error: Time format must be HH:MM"
              return 1
          end

          if test $target_hour -lt 0 -o $target_hour -ge 24
              echo "Error: Hour must be between 00 and 23"
              return 1
          end

          if test $target_minute -lt 0 -o $target_minute -ge 60
              echo "Error: Minute must be between 00 and 59"
              return 1
          end

          while true
              set -l current_time (date +"%H:%M")
              set -l current_hour (string split ':' $current_time)[1]
              set -l current_minute (string split ':' $current_time)[2]

              if test (math "$current_hour * 60 + $current_minute") -ge (math "$target_hour * 60 + $target_minute")
                  return 0
              end

              sleep 15
          end
        '';
      };
    };
  };
  # Spaceship ZSH alternative https://starship.rs/
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    enableFishIntegration = true;
    settings = {
      kubernetes.disabled = false;
    };
  };
  programs.nix-index = {
    enable = true;
    enableFishIntegration = true;
  };
  programs.ghostty = {
    enable = true;
    package = null; # Already installed via homebrew
    enableFishIntegration = true;
    settings = {
      theme = "light:Builtin Solarized Light,dark:Builtin Solarized Dark";
    };
  };
  programs.firefox = {
    enable = true;
    package = null; # Already installed via homebrew
  };
  programs.mise = {
    enable = true;
    enableFishIntegration = true;
    globalConfig.tools = {
      node = "lts";
      deno = "latest";
      python = "latest";
      pnpm = "latest";
    };
  };
}

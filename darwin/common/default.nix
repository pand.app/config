{ pkgs, ... }:
{
  imports = [
    ./system.nix
    ./homebrew.nix
  ];
  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment = {
    shells = with pkgs; [
      fish
      bash
      zsh
    ];
    systemPackages = [ ];
    systemPath = [ "/opt/homebrew/bin" ];
    pathsToLink = [ "/Applications" ];
    variables = {
      TERM = "xterm-256color";
    };
  };

  security.pam.enableSudoTouchIdAuth = true;

  # Necessary for using flakes on this system.
  nix.settings.experimental-features = "nix-command flakes";
  nix.package = pkgs.nix;
  nix.enable = false;
  nix.extraOptions = ''
    extra-platforms = x86_64-darwin aarch64-darwin
  '';
  nix.settings.trusted-users = [
    "root"
    "@admin"
  ];

  programs.zsh.enable = true;
  programs.fish.enable = true;

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 6;

  fonts.packages = [
    pkgs.fira-code # Font with ligatures for dev
    pkgs.nerd-fonts.fira-code # Same but patched for starship zsh
    pkgs.ubuntu_font_family
    pkgs.jetbrains-mono
  ];
}

{
  config,
  lib,
  pkgs,
  ...
}:

{
  # SSH hardening
  services.openssh = {
    enable = true;
    settings = {
      X11Forwarding = false;
      MaxAuthTries = 3;
      LoginGraceTime = 30;
    };
    extraConfig = ''
      AllowGroups wheel
      ClientAliveInterval 300
      ClientAliveCountMax 2
    '';
  };
  users.users."panda".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPVQvSaEdChCYfG29+ZwWztSn5Fo7LjPKDARtNItkUuH" # deploy
  ];

  # System security settings
  security = {
    auditd.enable = true;
    audit.enable = true;
    protectKernelImage = true;

    # PAM settings
    pam = {
      loginLimits = [
        {
          domain = "*";
          type = "hard";
          item = "maxlogins";
          value = 10;
        }
      ];
    };
  };

  # Enable automatic security updates
  system.autoUpgrade = {
    enable = true;
    allowReboot = false;
  };

  # System hardening
  boot.kernelParams = [
    "audit=1"
    "slab_nomerge"
    "init_on_alloc=1"
    "init_on_free=1"
  ];

  # Additional security packages
  environment.systemPackages = with pkgs; [
    usbguard
    aide # File integrity monitoring
  ];
}

{
  config,
  lib,
  pkgs,
  ...
}:

let
  # Path to the secrets directory (outside of the git repository)
  secretsDir = "/var/lib/secrets";
in
{
  # Create the secrets directory
  system.activationScripts.secretsDir = ''
    mkdir -p ${secretsDir}
    chmod 750 ${secretsDir}
    chown root:root ${secretsDir}
  '';
}

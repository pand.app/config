{
  config,
  lib,
  pkgs,
  ...
}:
{
  # Configure Nginx virtual host for Transmission
  services.nginx.virtualHosts."transmission.pand.app" = {
    forceSSL = true;
    enableACME = true;
    # Then add the main location
    locations."/" = {
      proxyPass = "http://127.0.0.1:9091";
      proxyWebsockets = true;
    };
  };

  # Ensure .env file exists but don't manage its contents
  system.activationScripts.transmission-env = ''
    touch /etc/container-compose/transmission/.env
    chown panda:users /etc/container-compose/transmission/.env
    chmod 600 /etc/container-compose/transmission/.env
  '';

  # Create Transmission docker-compose configuration
  environment.etc."container-compose/transmission/docker-compose.yml".text = ''
    services:
      transmission:
        image: docker.io/linuxserver/transmission:latest
        volumes:
          - /var/lib/transmission/config:/config
          - /var/lib/transmission/downloads:/downloads
          - /var/lib/transmission/watch:/watch
        environment:
          - PUID=1000
          - PGID=100
          - TZ=Europe/Paris
        env_file: .env
        ports:
          - 127.0.0.1:9091:9091
          - 51413:51413
          - 51413:51413/udp
        restart: unless-stopped
  '';

  # Create directories for Transmission data
  system.activationScripts.transmission-dirs = ''
    mkdir -p /var/lib/transmission/{config,downloads,watch}
    mkdir -p /var/lib/transmission/downloads/{complete,incomplete}
    chmod 755 /var/lib/transmission/watch
    chown -R panda:users /var/lib/transmission
  '';

  # Systemd service for Transmission docker-compose
  systemd.services.transmission-compose = {
    description = "Transmission Docker Compose Service";
    wantedBy = [ "multi-user.target" ];
    requires = [ "podman.service" ];
    after = [
      "podman.service"
      "network.target"
    ];
    path = [
      pkgs.podman
      pkgs.podman-compose
    ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      WorkingDirectory = "/etc/container-compose/transmission";
      ExecStart = "${pkgs.podman-compose}/bin/podman-compose up -d";
      ExecStop = "${pkgs.podman-compose}/bin/podman-compose down";
    };
  };

  # Configure firewall rules for Transmission
  networking.firewall = {
    allowedTCPPorts = [ 51413 ];
    allowedUDPPorts = [ 51413 ];
  };
}

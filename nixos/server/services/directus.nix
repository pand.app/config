{
  config,
  lib,
  pkgs,
  ...
}:
{
  # Configure Nginx virtual host for directus
  services.nginx.virtualHosts."directus.pand.app" = {
    forceSSL = true;
    enableACME = true;
    # Then add the main location
    locations."/" = {
      proxyPass = "http://127.0.0.1:8055";
      proxyWebsockets = true;
    };
  };

  # Ensure .env file exists but don't manage its contents
  system.activationScripts.directus-env = ''
    touch /etc/container-compose/directus/.env
    chown panda:users /etc/container-compose/directus/.env
    chmod 600 /etc/container-compose/directus/.env
  '';
  # Create directus docker-compose configuration
  environment.etc."container-compose/directus/docker-compose.yml".text = ''
    services:
      directus:
        image: docker.io/directus/directus:11.4
        volumes:
          - /var/lib/directus/uploads:/directus/uploads
          - /var/lib/directus/extensions:/directus/extensions
        depends_on:
          - cache
          - database
        env_file: .env
        environment:
          KEY: $DIRECTUS_KEY
          SECRET: $DIRECTUS_SECRET
          DB_CLIENT: 'pg'
          DB_HOST: $PGHOST
          DB_PORT: $PGPORT
          DB_DATABASE: $PGDATABASE
          DB_USER: $PGUSER
          DB_PASSWORD: $PGPASSWORD
          PORT: 8055

          CACHE_ENABLED: 'true'
          CACHE_STORE: 'redis'
          REDIS: 'redis://cache:6379'

          PUBLIC_URL: 'https://directus.pand.app'

          EMAIL_FROM: directus@$DOMAIN
          EMAIL_TRANSPORT: smtp
          EMAIL_SMTP_HOST: smtp.tem.scw.cloud
          EMAIL_SMTP_PASSWORD: $SCALEWAY_SECRET_KEY
          EMAIL_SMTP_PORT: "465"
          EMAIL_SMTP_SECURE: "true"
          EMAIL_SMTP_USER: $SCALEWAY_SMTP_USER
          EMAIL_SMTP_NAME: $DOMAIN

          STORAGE_LOCATIONS: "cloud"
          STORAGE_CLOUD_DRIVER: "s3"
          STORAGE_CLOUD_BUCKET: $SCALEWAY_BUCKETNAME
          STORAGE_CLOUD_ENDPOINT: $SCALEWAY_ENDPOINT
          STORAGE_CLOUD_KEY: $SCALEWAY_ACCESS_KEY
          STORAGE_CLOUD_REGION: $SCALEWAY_REGION
          STORAGE_CLOUD_SECRET: $SCALEWAY_SECRET_KEY
          STORAGE_CLOUD_ROOT: "/assets"
          CORS_ENABLED: "true"
          CORS_ORIGIN: "true"
        ports:
          - 127.0.0.1:8055:8055
      database:
        image: docker.io/kartoza/postgis:16-3.4
        volumes:
          - /var/lib/directus/postgresql/database:/var/lib/postgresql
        env_file: .env
        environment:
          POSTGRES_USER: $PGUSER
          POSTGRES_PASS: $PGPASSWORD
          POSTGRES_DBNAME: $PGDATABASE
          PGUSER: postgres
          PGGROUP: postgres

      cache:
        image: docker.io/redis:7.4

      backup:
        restart: "no"
        image: docker.io/pkgxdev/pkgx
        command: |
          bash -c '
          set -e
          echo "[$(date)] Starting Directus backup process...";
          echo "[$(date)] Setting up rclone configuration...";
          mkdir -p $(dirname $$RCLONE_CONFIG_PATH);
          touch $$RCLONE_CONFIG_PATH;
          echo "[$(date)] Creating rclone config file..."
          cat > "$$RCLONE_CONFIG_PATH" << EOF
          [scaleway]
          type = s3
          provider = Scaleway
          env_auth = false
          access_key_id = $$SCALEWAY_ACCESS_KEY
          secret_access_key = $$SCALEWAY_SECRET_KEY
          region = $$SCALEWAY_REGION
          endpoint = $$SCALEWAY_ENDPOINT
          acl = private
          server_side_encryption =
          location_constraint =
          storage_class = GLACIER
          EOF

          echo "[$(date)] Setting up backup environment..."
          CURRENT_DATETIME=$(date +"%Y%m%d_%H%M%S")
          export RETENTION_DAYS=30
          export BACKUP_DIR="directus_backup_$$CURRENT_DATETIME"

          echo "[$(date)] Creating backup directory: $$BACKUP_DIR"
          mkdir -p "$$BACKUP_DIR"

          echo "[$(date)] Backing up database..."
          if ! pkgx pg_dump -h database -U "$$PGUSER" -d "$$PGDATABASE" > "$$BACKUP_DIR/database.sql"; then
            echo "[$(date)] ERROR: Database backup failed!"
            exit 1
          fi

          echo "[$(date)] Backing up environment variables..."
          printenv > "$$BACKUP_DIR/environment.txt"

          echo "[$(date)] Copying uploads and extensions..."
          if [ -d "/directus/uploads" ]; then
            cp -r /directus/uploads "$$BACKUP_DIR/"
          else
            echo "[$(date)] WARNING: Uploads directory not found"
          fi

          if [ -d "/directus/extensions" ]; then
            cp -r /directus/extensions "$$BACKUP_DIR/"
          else
            echo "[$(date)] WARNING: Extensions directory not found"
          fi

          echo "[$(date)] Creating archive..."
          tar -czf "$$BACKUP_DIR.tar.gz" -C "$$BACKUP_DIR" .

          echo "[$(date)] Uploading to S3..."
          if ! pkgx rclone copy --progress --no-check-certificate "$$BACKUP_DIR.tar.gz" "scaleway:$$SCALEWAY_BUCKETNAME/backup/"; then
            echo "[$(date)] ERROR: Upload to S3 failed!"
            exit 1
          fi

          echo "[$(date)] Cleaning up local files..."
          rm -rf "$$BACKUP_DIR"
          rm -f "$$BACKUP_DIR.tar.gz"

          echo "[$(date)] Removing old backups..."
          pkgx rclone delete --min-age "$${RETENTION_DAYS}d" "scaleway:$$SCALEWAY_BUCKETNAME/backup/"

          echo "[$(date)] Backup completed successfully!"
          '
        env_file: .env
        environment:
          RCLONE_CONFIG_PATH: /root/.config/rclone/rclone.conf
          RCLONE_VERBOSE: 2
        deploy:
          restart_policy:
            condition: none
  '';

  # Create directories for directus data
  system.activationScripts.directus-dirs = ''
    mkdir -p /var/lib/directus/{extensions,uploads,rclone}
    mkdir -p /var/lib/directus/postgresql/database
    chmod -R 770 /var/lib/directus/{extensions,uploads,postgresql,rclone}
    chown -R panda:podman /var/lib/directus/{extensions,uploads,rclone}
    chown -R 101:103 /var/lib/directus/postgresql
  '';

  # Systemd service for directus docker-compose
  systemd.services.directus-compose = {
    description = "directus Docker Compose Service";
    wantedBy = [ "multi-user.target" ];
    requires = [ "podman.service" ];
    after = [
      "podman.service"
      "network.target"
    ];
    path = [
      pkgs.podman
      pkgs.podman-compose
    ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      WorkingDirectory = "/etc/container-compose/directus";
      ExecStart = "${pkgs.podman-compose}/bin/podman-compose up -d";
      ExecStop = "${pkgs.podman-compose}/bin/podman-compose down";
    };
  };

  # Create a systemd timer for directus backups
  systemd.timers.directus-backup = {
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnCalendar = "*-*-* 02:00:00";
      Persistent = true;
    };
  };

  # Create a systemd service for directus backups
  systemd.services.directus-backup = {
    description = "Directus Backup Service";
    requires = [ "podman.service" ];
    wantedBy = [ "multi-user.target" ];
    after = [
      "podman.service"
      "network.target"
    ];
    path = [ 
        pkgs.podman
        pkgs.podman-compose
    ];
    serviceConfig = {
      Type = "oneshot";
      WorkingDirectory = "/etc/container-compose/directus";
      ExecStart = "${pkgs.podman-compose}/bin/podman-compose run --rm backup";
    };
  };
}

{
  config,
  lib,
  pkgs,
  ...
}:

{
  users.users.nginx.extraGroups = [ "acme" ];
  # Configure Nginx with SSL
  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
  };

  # Enable ACME for Let's Encrypt with HTTP validation
  security.acme = {
    acceptTerms = true;
    defaults = {
      email = "hi@panda.blue";
      server = "https://acme-v02.api.letsencrypt.org/directory";
    };
  };
}
